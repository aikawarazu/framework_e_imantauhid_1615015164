<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admin';
    protected $fillable = ['id','nama','notlp','email','alamat','pengguna_id'];

    public function pengguna(){
    	return $this->belongsTo(Pengguna::Class);
    }
}

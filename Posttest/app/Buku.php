<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku' ;
    protected $fillable = ['id','judul','kategori_id','penerbit','tanggal'];

    public function kategori(){
    	return $this->belongsTo(Kategori::Class);
    }

    public function pembeli(){
    	return $this->belongsToMany(Pembeli::Class)->withPivot('id');
    }

    public function penulis(){
    	return $this->belongsToMany(Penulis::Class)->withPivot('id');
    }

}

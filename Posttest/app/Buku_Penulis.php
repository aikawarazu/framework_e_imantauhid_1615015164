<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku_Penulis extends Model
{
    protected $table = 'buku_penulis' ;
    protected $fillable = ['id','penulis_id','buku_id'];
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku; 
use App\Penulis;
use App\Kategori;

class BukuController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

       public function coba(){
        $buku = Kategori::find(1)->buku()->where('judul','Harry Potter')->first();
        dd($buku);
    }

    public function awal()
    {
     $datac = Buku::all();
     return view('buku.app', compact('datac'));
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah()
    {
        $author = Penulis::all('nama','id')->pluck('nama','id');
        $categories = Kategori::all(['deskripsi','id'])->pluck('deskripsi','id');
        return view('buku.tambah', compact('author'), compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan(Request $input)
    {
        $this->validate($input, array
            (
                'judul' => 'required',
                'kategori' => 'required|integer',
                'penerbit'=> 'required',
                'tanggal' => 'required',
            ));
        
        $buku = new buku();
        $buku->judul = $input->judul;
        $buku->kategori_id = $input->kategori;
        $buku->penerbit = $input->penerbit;
        $buku->tanggal = $input->tanggal;
        $buku->save();
        $buku->penulis()->attach($input->penulis);
        return redirect('buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view("buku.menu");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = Penulis::all('nama','id')->pluck('nama','id');
        $categories = Kategori::all(['deskripsi','id'])->pluck('deskripsi','id');
        $buku = Buku::find($id);
        return view('buku.edit', compact('author'), compact('categories'))->with(array('buku'=>$buku));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($input, array 
        ( 
            'judul' => 'required', 
            'kategori' => 'required', 
            'penerbit'=> 'required', 
            'tanggal' => 'required', 
        )); 

        $buku = Buku::find($id);
        $id2 = $buku->penulis->first()->pivot->id;
        $buku->judul = $input->judul;
        $buku->kategori_id = $input->kategori;
        $buku->penerbit = $input->penerbit;
        $buku->tanggal = $input->tanggal;
        $buku->save();$buku->penulis()->newPivotStatement()->where('id', $id2)->update(['penulis_id' => $input->penulis]);
//$buku->penulis()->attach($input->penulis);
        return redirect('buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $buku = Buku::find($id);
        $buku->penulis()->detach();
        $buku->delete();
        return redirect('buku');
    }
}

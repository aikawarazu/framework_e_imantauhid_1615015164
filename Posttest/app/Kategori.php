<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori' ;
    protected $fillable = ['id','deskripsi'];

    public function buku(){
    	return $this->hasOne(Buku::Class);
    }
}

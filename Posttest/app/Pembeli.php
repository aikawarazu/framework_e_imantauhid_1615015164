<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $table = 'pembeli' ;
    protected $fillable = ['id','nama','alamat','notlp'];

    public function buku(){
    	return $this->belongsToMany(Buku::Class)->withPivot('id');
    }

}

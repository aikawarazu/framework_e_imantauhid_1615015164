<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pengguna extends Authenticatable
{
    protected $table = 'pengguna';
    protected $filable =['username','password','level'];
    
    protected $hidden =['password','remember_token' ];

public function Admin(){
		return $this->hasOne(Admin::class);
	}
}

@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Kategori
		<div class="pull-right">
			<a href="{{ url('kategori/tambah')}}" class="btn btn-xs btn-primary">Tambah Kategori</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Deskripsi</td>
				</tr>
				@foreach($kategori as $Kategori)
					
				<tr>
					<td >{{ $Kategori->deskripsi }}</td>
					
					<td>
					<a href="{{url('kategori/edit/'.$Kategori->id)}}" class="btn btn-xs btn-success">Edit</a>
					<a href="{{url('kategori/hapus/'.$Kategori->id)}}" class="btn btn-xs btn-danger">Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection
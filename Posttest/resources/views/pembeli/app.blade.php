<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
</head>
<body>
@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Pembeli
		<div class="pull-right">
			<a href="{{ url('pembeli/tambah')}}"  class="btn btn-xs btn-primary">Tambah Data</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Nama</td>
					<td>No Telepon</td>
					<td>Email</td>
					<td>Alamat</td>
				</tr>
				@foreach($pembeli as $Pembeli)
					
				<tr>
					<td >{{ $Pembeli->nama }}</td>
					<td >{{ $Pembeli->notlp}}</td>
					<td >{{ $Pembeli->email }}</td>
					<td >{{ $Pembeli->alamat}}</td>
					<td > 
					
						<a href="{{url('pembeli/edit/'.$Pembeli->id)}} " class="btn btn-xs btn-success">Edit</a>
						<a href="{{url('pembeli/hapus/'.$Pembeli->id)}}" class="btn btn-xs btn-danger">Hapus</a>
					
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection

</body>
</html>


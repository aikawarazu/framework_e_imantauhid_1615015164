
<!DOCTYPE html>
<html>
<head>
	<title>Tambah Pembeli</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
</head>
<body>

@extends('master')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Tambah Data Pembeli
				</div>
				<div class="panel-body">
					{!! Form::open(['url'=>'pembeli/simpan','class'=>'form-horizontal']) !!}
						@include('pembeli.form')

						<div style="width:100%;text-align:center;">
							<button class="btn btn-primary"><i class="fa fa-save"></i>
							Simpan</button>
							<input type="button" value="Reset" class="btn btn-danger" onClick="window.location.reload()"/>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection 


</body>
</html>


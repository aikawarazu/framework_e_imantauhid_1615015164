<?php
Route::get('/', function () {
	return view('welcome');
});

Route::get('/pengguna','PenggunaController@awal');
Route::get('/save','PenggunaController@simpan');

Route::get('/buku','BukuController@awal');
Route::get('/buku/tambah','BukuController@tambah');
Route::post('/buku/simpan','BukuController@simpan');
Route::get('/buku/edit/{buku}','BukuController@edit');
Route::post('/buku/update/{buku}','BukuController@update');
Route::get('/buku/hapus/{buku}','BukuController@hapus');

Route::post('/penulis/simpan','PenulisController@simpan');
Route::get('/penulis','PenulisController@awal');
Route::get('/penulis/edit/{penulis}','PenulisController@edit');
Route::post('/penulis/update/{penulis}','PenulisController@update');
Route::get('/penulis/hapus/{penulis}','PenulisController@hapus');
Route::get('/penulis/tambah','PenulisController@tambah');

Route::get('/kategori','KategoriController@awal');
Route::post('/kategori/simpan','KategoriController@simpan');
Route::get('/kategori/edit/{simpan}','KategoriController@edit');
Route::post('/kategori/update/{simpan}','KategoriController@update');
Route::get('/kategori/hapus/{simpan}','KategoriController@hapus');
Route::get('/kategori/tambah','KategoriController@tambah');

Route::get('/pembeli/tambah','PembeliController@tambah');
Route::post('/pembeli/simpan','PembeliController@simpan');
Route::get('/pembeli','PembeliController@awal');
Route::get('/pembeli/edit/{pembeli}','PembeliController@edit');
Route::post('/pembeli/update/{pembeli}','PembeliController@update');
Route::get('/pembeli/hapus/{pembeli}','PembeliController@hapus');

Route::get('/admin','AdminController@awal');
Route::get('/admin/tambah','AdminController@simpan');
Route::post('/admin/simpan','AdminController@simpan');
Route::get('/admin/lihat','AdminController@show');
Route::get('/admin/edit/{admin}','AdminController@edit');
Route::post('/admin/update/{admin}','AdminController@update');
Route::get('/admin/hapus/{admin}','AdminController@hapus');

Route::get('/','AuthController@index');
Route::get('/login','AuthController@formLogin');
Route::post('/login','AuthController@proses');
Route::get('/logout','AuthController@logout');
